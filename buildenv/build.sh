#!/bin/bash
CURDIR=$(pwd)
CURL_VERSION=7.64.1
wget -O curl.tar.bz2 https://curl.haxx.se/download/curl-$CURL_VERSION.tar.bz2

tar -jxvf curl.tar.bz2
cd curl-$CURL_VERSION
mkdir build
cd build && mkdir usrlibs
CURL_INSTALL_DIRS=$(pwd)/usrlibs
../configure --prefix=$CURL_INSTALL_DIRS
make install -j8

echo "Installed to ${CURL_INSTALL_DIRS}" 
cd ${CURDIR}
echo "Source dir: $PWD"

mkdir build
cd build
apt-get install gcc-4.8 g++-4.8 -y
export CC=gcc-4.8
export CXX=g++-4.8
cmake -DCMAKE_CXX_FLAGS="-Wall -I${CURL_INSTALL_DIRS}/include -DCURL_STATICLIB" -DUSE_LIBCURL_FROM="${CURL_INSTALL_DIRS}/lib/libcurl.a" -G "Ninja" ..
export LD_LIBRARY_PATH=$CURL_INSTALL_DIRS/libs/:$LD_LIBRARY_PATH
ninja -j8
cd ${CURDIR}
rm -rf curl.tar.bz2 curl-$CURL_VERSION/