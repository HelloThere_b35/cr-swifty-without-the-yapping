#include <errno.h>
#define __USE_MISC
#include <fcntl.h> 
#include <string.h>
#include <termios.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>
#include <linux/types.h>
// #include <libtpms/tpm_library.h>
#include "tpm_vendor_cmds.h"
#include <stdarg.h>
#include <stdint.h>
//TODO(all other ecsmasher members): Cleanup comments, and legacy code.
#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))
// CRSwifty terminal setup, and exploit
typedef struct {
    int vendor_cmd;
    size_t name_length;
    const char* name;
    
} VendorCommand;


const char* moveCharLeft3 = "\x1B[3D";
#define VC_COMMAND_EXIT 65537
static VendorCommand vendor_cmds[] = {
{EXTENSION_AES,sizeof("EXTENSION_AES"), "EXTENSION_AES"},
{EXTENSION_HASH,sizeof("EXTENSION_HASH"), "EXTENSION_HASH"},
{EXTENSION_RSA,sizeof("RSA"), "RSA"},
{EXTENSION_ECC,sizeof("ECC"), "ECC"},
{EXTENSION_FW_UPGRADE,sizeof("FW_UPGRADE"), "FW_UPGRADE"},
{EXTENSION_HKDF,sizeof("HKDF"), "HKDF"},
{EXTENSION_ECIES,sizeof("ECIES"), "ECIES"},
{EXTENSION_POST_RESET,sizeof("POST_RESET"), "POST_RESET"},
{LAST_EXTENSION_COMMAND,sizeof("LAST_EXTENSION_COMMAND"), "LAST_EXTENSION_COMMAND"},
{VENDOR_CC_GET_LOCK,sizeof("VENDOR_CC_GET_LOCK"), "VENDOR_CC_GET_LOCK"},
{VENDOR_CC_SET_LOCK,sizeof("VENDOR_CC_SET_LOCK"), "VENDOR_CC_SET_LOCK"},
{VENDOR_CC_SYSINFO,sizeof("VENDOR_CC_SYSINFO"), "VENDOR_CC_SYSINFO"},
{VENDOR_CC_IMMEDIATE_RESET,sizeof("VENDOR_CC_IMMEDIATE_RESET"), "VENDOR_CC_IMMEDIATE_RESET"},
{VENDOR_CC_INVALIDATE_INACTIVE_RW,sizeof("VENDOR_CC_INVALIDATE_INACTIVE_RW"), "VENDOR_CC_INVALIDATE_INACTIVE_RW"},
{VENDOR_CC_COMMIT_NVMEM,sizeof("VENDOR_CC_COMMIT_NVMEM"), "VENDOR_CC_COMMIT_NVMEM"},
{VENDOR_CC_REPORT_TPM_STATE,sizeof("VENDOR_CC_REPORT_TPM_STATE"), "VENDOR_CC_REPORT_TPM_STATE"},
{VENDOR_CC_TURN_UPDATE_ON,sizeof("VENDOR_CC_TURN_UPDATE_ON"), "VENDOR_CC_TURN_UPDATE_ON"},
{VENDOR_CC_GET_BOARD_ID,sizeof("GET_BOARD_ID"), "GET_BOARD_ID"},
{VENDOR_CC_SET_BOARD_ID,sizeof("VENDOR_CC_SET_BOARD_ID"), "VENDOR_CC_SET_BOARD_ID"},
{VENDOR_CC_U2F_APDU,sizeof("VENDOR_CC_U2F_APDU"), "VENDOR_CC_U2F_APDU"},
{VENDOR_CC_POP_LOG_ENTRY,sizeof("VENDOR_CC_POP_LOG_ENTRY"), "VENDOR_CC_POP_LOG_ENTRY"},
{VENDOR_CC_GET_REC_BTN,sizeof("VENDOR_CC_GET_REC_BTN"), "VENDOR_CC_GET_REC_BTN"},
{VENDOR_CC_RMA_CHALLENGE_RESPONSE,sizeof("VENDOR_CC_RMA_CHALLENGE_RESPONSE"), "VENDOR_CC_RMA_CHALLENGE_RESPONSE"},
{VENDOR_CC_DISABLE_FACTORY,sizeof("VENDOR_CC_DISABLE_FACTORY"), "VENDOR_CC_DISABLE_FACTORY"},
{VENDOR_CC_CCD,sizeof("VENDOR_CC_CCD"), "VENDOR_CC_CCD"},
{VENDOR_CC_GET_ALERTS_DATA,sizeof("VENDOR_CC_GET_ALERTS_DATA"), "VENDOR_CC_GET_ALERTS_DATA"},
{VENDOR_CC_SPI_HASH,sizeof("VENDOR_CC_SPI_HASH"), "VENDOR_CC_SPI_HASH"},
{VENDOR_CC_PINWEAVER,sizeof("VENDOR_CC_PINWEAVER"), "VENDOR_CC_PINWEAVER"},
{VENDOR_CC_RESET_FACTORY,sizeof("VENDOR_CC_RESET_FACTORY"), "VENDOR_CC_RESET_FACTORY"},
{VENDOR_CC_WP,sizeof("VENDOR_CC_WP"), "VENDOR_CC_WP"},
{VENDOR_CC_TPM_MODE,sizeof("VENDOR_CC_TPM_MODE"), "VENDOR_CC_TPM_MODE"},
{VENDOR_CC_SN_SET_HASH,sizeof("VENDOR_CC_SN_SET_HASH"), "VENDOR_CC_SN_SET_HASH"},
{VENDOR_CC_SN_INC_RMA,sizeof("VENDOR_CC_SN_INC_RMA"), "VENDOR_CC_SN_INC_RMA"},
{VENDOR_CC_GET_PWR_BTN,sizeof("VENDOR_CC_GET_PWR_BTN"), "VENDOR_CC_GET_PWR_BTN"},
{VENDOR_CC_U2F_GENERATE,sizeof("VENDOR_CC_U2F_GENERATE"), "VENDOR_CC_U2F_GENERATE"},
{VENDOR_CC_U2F_SIGN,sizeof("VENDOR_CC_U2F_SIGN"), "VENDOR_CC_U2F_SIGN"},
{VENDOR_CC_U2F_ATTEST,sizeof("VENDOR_CC_U2F_ATTEST"), "VENDOR_CC_U2F_ATTEST"},
{VENDOR_CC_FLOG_TIMESTAMP,sizeof("VENDOR_CC_FLOG_TIMESTAMP"), "VENDOR_CC_FLOG_TIMESTAMP"},
{VENDOR_CC_ENDORSEMENT_SEED,sizeof("VENDOR_CC_ENDORSEMENT_SEED"), "VENDOR_CC_ENDORSEMENT_SEED"},
{VENDOR_CC_U2F_MODE,sizeof("VENDOR_CC_U2F_MODE"), "VENDOR_CC_U2F_MODE"},
{VENDOR_CC_DRBG_TEST,sizeof("VENDOR_CC_DRBG_TEST"), "VENDOR_CC_DRBG_TEST"},
{VENDOR_CC_TRNG_TEST,sizeof("VENDOR_CC_TRNG_TEST"), "VENDOR_CC_TRNG_TEST"},
{VENDOR_CC_GET_BOOT_MODE,sizeof("VENDOR_CC_GET_BOOT_MODE"), "VENDOR_CC_GET_BOOT_MODE"},
{VENDOR_CC_RESET_EC,sizeof("VENDOR_CC_RESET_EC"), "VENDOR_CC_RESET_EC"},
{VENDOR_CC_SEED_AP_RO_CHECK,sizeof("VENDOR_CC_SEED_AP_RO_CHECK"), "VENDOR_CC_SEED_AP_RO_CHECK"},
{VENDOR_CC_FIPS_CMD,sizeof("VENDOR_CC_FIPS_CMD"), "VENDOR_CC_FIPS_CMD"},
{VENDOR_CC_GET_AP_RO_HASH,sizeof("VENDOR_CC_GET_AP_RO_HASH"), "VENDOR_CC_GET_AP_RO_HASH"},
{VENDOR_CC_GET_AP_RO_STATUS,sizeof("VENDOR_CC_GET_AP_RO_STATUS"), "VENDOR_CC_GET_AP_RO_STATUS"},
{VENDOR_CC_AP_RO_VALIDATE,sizeof("VENDOR_CC_AP_RO_VALIDATE"), "VENDOR_CC_AP_RO_VALIDATE"},
{VENDOR_CC_DS_DIS_TEMP,sizeof("VENDOR_CC_DS_DIS_TEMP"), "VENDOR_CC_DS_DIS_TEMP"},
{VENDOR_CC_USER_PRES,sizeof("VENDOR_CC_USER_PRES"), "VENDOR_CC_USER_PRES"},
{VENDOR_CC_POP_LOG_ENTRY_MS,sizeof("VENDOR_CC_POP_LOG_ENTRY_MS"), "VENDOR_CC_POP_LOG_ENTRY_MS"},
{VENDOR_CC_GET_AP_RO_VERIFY_SETTING,sizeof("VENDOR_CC_GET_AP_RO_VERIFY_SETTING"), "VENDOR_CC_GET_AP_RO_VERIFY_SETTING"},
{VENDOR_CC_SET_AP_RO_VERIFY_SETTING,sizeof("VENDOR_CC_SET_AP_RO_VERIFY_SETTING"), "VENDOR_CC_SET_AP_RO_VERIFY_SETTING"},
{VENDOR_CC_SET_CAPABILITY,sizeof("VENDOR_CC_SET_CAPABILITY"), "VENDOR_CC_SET_CAPABILITY"},
{VENDOR_CC_GET_TI50_STATS,sizeof("VENDOR_CC_GET_TI50_STATS"), "VENDOR_CC_GET_TI50_STATS"},
{VENDOR_CC_GET_CRASHLOG,sizeof("VENDOR_CC_GET_CRASHLOG"), "VENDOR_CC_GET_CRASHLOG"},
{VENDOR_CC_GET_CONSOLE_LOGS,sizeof("VENDOR_CC_GET_CONSOLE_LOGS"), "VENDOR_CC_GET_CONSOLE_LOGS"},
{VENDOR_CC_GET_FACTORY_CONFIG,sizeof("VENDOR_CC_GET_FACTORY_CONFIG"), "VENDOR_CC_GET_FACTORY_CONFIG"},
{VENDOR_CC_SET_FACTORY_CONFIG,sizeof("VENDOR_CC_SET_FACTORY_CONFIG"), "VENDOR_CC_SET_FACTORY_CONFIG"},
{VENDOR_CC_GET_TIME,sizeof("VENDOR_CC_GET_TIME"), "VENDOR_CC_GET_TIME"},
{VENDOR_CC_GET_BOOT_TRACE,sizeof("VENDOR_CC_GET_BOOT_TRACE"), "VENDOR_CC_GET_BOOT_TRACE"},
{VENDOR_CC_GET_CR50_METRICS,sizeof("VENDOR_CC_GET_CR50_METRICS"), "VENDOR_CC_GET_CR50_METRICS"},
{LAST_VENDOR_COMMAND,sizeof("LAST_VENDOR_COMMAND"), "LAST_VENDOR_COMMAND"},
{VC_COMMAND_EXIT, sizeof("EXIT"), "EXIT"}
};

int setupTTY(int fd, int speed, int parity) {

    struct termios tty;
    tcgetattr(fd, &tty);
    cfsetospeed(&tty, speed);
    cfsetispeed(&tty, speed);
    tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;
    //https://stackoverflow.com/questions/6947413/how-to-open-read-and-write-from-serial-port-in-c

    tty.c_iflag &= ~IGNBRK; // disable break processing
    tty.c_lflag = 0;        // no signaling chars, no echo,
                            // no canonical processing
    // tty.c_oflag = 0;        // no remapping, no delays
    tty.c_cc[VMIN] = 0;     // read doesn't block
    tty.c_cc[VTIME] = 5;    // 0.5 seconds read timeout

    tty.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

    tty.c_cflag |= (CLOCAL | CREAD);   // ignore modem controls,
                                       // enable reading
    // tty.c_cflag &= ~(PARENB | PARODD); // shut off parity
    tty.c_cflag |= parity;
    tty.c_cflag &= ~CSTOPB;
    tty.c_cflag &= ~CRTSCTS;
    tcsetattr (fd, TCSANOW, &tty);
    return 0;
}
struct upgrade_pkt {
	__be16	tag;
	__be32	length;
	__be32	ordinal;
	__be16	subcmd;
	union {
		/*
		 * Upgrade PDUs as opposed to all other vendor and extension
		 * commands include two additional fields in the header.
		 */
		struct {
			__be32	digest;
			__be32	address;
			char data[0];
		} upgrade;
		struct {
			char data[0];
		} command;
	};
} __packed;
//The above code came from: https://stackoverflow.com/questions/6947413/how-to-open-read-and-write-from-serial-port-in-c ^
static int tpm_send_pkt(int tpmFD, unsigned int digest,
			unsigned int addr, const void *data, int size,
			void *response, size_t *response_size,
			uint16_t subcmd)
{
	/* Used by transfer to /dev/tpm0 */
	static uint8_t outbuf[2048];
	static uint8_t raw_response[2048 +
		sizeof(struct upgrade_pkt)];
	struct upgrade_pkt *out = (struct upgrade_pkt *)outbuf;
	int len, done;
	int response_offset = offsetof(struct upgrade_pkt, command.data);
	void *payload;
	size_t header_size;
	uint32_t rv;
	const size_t rx_size = sizeof(raw_response);

	// debug("%s: sending to %#x %d bytes\n", __func__, addr, size);

	out->tag = htobe16(0x8001);
	out->subcmd = htobe16(subcmd);

	if (subcmd <= LAST_EXTENSION_COMMAND)
		out->ordinal = htobe32(0xbaccd00a);
	else
		out->ordinal = htobe32(TPM_CC_VENDOR_BIT_MASK);

	if (subcmd == EXTENSION_FW_UPGRADE) {
		/* FW Upgrade PDU header includes a couple of extra fields. */
		out->upgrade.digest = digest;
		out->upgrade.address = htobe32(addr);
		header_size = offsetof(struct upgrade_pkt, upgrade.data);
	} else {
		header_size = offsetof(struct upgrade_pkt, command.data);
	}

	payload = outbuf + header_size;
	len = size + header_size;

	out->length = htobe32(len);
	memcpy(payload, data, size);

	done = write(tpmFD, out, len);


	if (done < 0) {
		perror("Could not write to TPM");
		return -1;
	} else if (done != len) {
		fprintf(stderr, "Error: Wrote %d bytes, expected to write %d\n",
			done, len);
		return -1;
	}


	int read_count;

	len = 0;
	do {
		uint8_t *rx_buf = raw_response + len;
		size_t rx_to_go = rx_size - len;

		read_count = read(tpmFD, rx_buf, rx_to_go);

		len += read_count;
	} while (read_count);
	len = len - response_offset;
	if (len < 0) {
		fprintf(stderr, "Problems reading from TPM, got %d bytes\n",
			len + response_offset);
		return -1;
	}

	if (response && response_size) {
		len = MIN(len, *response_size);
		memcpy(response, raw_response + response_offset, len);
		*response_size = len;
	}

	/* Return the actual return code from the TPM response header. */
	memcpy(&rv, &((struct upgrade_pkt *)raw_response)->ordinal, sizeof(rv));
	rv = be32toh(rv);

	/* Clear out vendor command return value offset.*/
	if ((rv & VENDOR_RC_ERR) == VENDOR_RC_ERR)
		rv &= ~VENDOR_RC_ERR;

	return rv;
}
//gsctool.c But is adapted for crswifty.
uint32_t send_vendor_command(int tpmFD,
			     uint16_t subcommand,
			     const void *command_body,
			     size_t command_body_size,
			     void *response,
			     size_t *response_size)
{
	int32_t rv;
//  We don't need this code since its for updating over usb. However we aren't planning to send vendor commands over usb.
	// if (td->ep_type == usb_xfer) {
	// 	/*
	// 	 * When communicating over USB the response is always supposed
	// 	 * to have the result code in the first byte of the response,
	// 	 * to be stripped from the actual response body by this
	// 	 * function.
	// 	 */
	// 	uint8_t temp_response[MAX_RX_BUF_SIZE];
	// 	size_t max_response_size;

	// 	if (!response_size) {
	// 		max_response_size = 1;
	// 	} else if (*response_size < (sizeof(temp_response))) {
	// 		max_response_size = *response_size + 1;
	// 	} else {
	// 		fprintf(stderr,
	// 			"Error: Expected response too large (%zd)\n",
	// 			*response_size);
	// 		/* Should happen only when debugging. */
	// 		exit(update_error);
	// 	}

	// 	ext_cmd_over_usb(&td->uep, subcommand,
	// 			 command_body, command_body_size,
	// 			 temp_response, &max_response_size);
	// 	if (!max_response_size) {
	// 		/*
	// 		 * we must be talking to an older Cr50 firmware, which
	// 		 * does not return the result code in the first byte
	// 		 * on success, nothing to do.
	// 		 */
	// 		if (response_size)
	// 			*response_size = 0;
	// 		rv = 0;
	// 	} else {
	// 		rv = temp_response[0];
	// 		if (response_size) {
	// 			*response_size = max_response_size - 1;
	// 			memcpy(response,
	// 			       temp_response + 1, *response_size);
	// 		}
	// 	}
	// } else {

		rv = tpm_send_pkt(tpmFD, 0, 0,
				  command_body, command_body_size,
				  response, response_size, subcommand);

		
	// }

	return rv; /* This will be converted into uint32_t */
}
// s




struct vc_board_id {
	uint32_t type;		/* Board type */
	uint32_t type_inv;	/* Board type (inverted) */
	uint32_t flags;		/* Flags */
};
void handle_command (int tpmDescriptor, char *commandBuf, size_t commandBufLength) {
    // printf("Command buffer length: %d\n", commandBufLength);
    
    VendorCommand* chosenVC = NULL;
    for (int i = 0; i <65; i++) {
        VendorCommand vc = vendor_cmds[i];
        if (vc.name_length != commandBufLength) {
            // There is no way that this can be equal. Search for more possibilities.
            // printf("Exiting because no matching length;");
            continue;
        }
        int compared = strncasecmp(commandBuf, vc.name, vc.name_length-1);
        if (!compared) {
            // printf("Handling command, just a sec...\n");
            chosenVC = &vc;
            break;
        };
    }
    if (!chosenVC) {
        printf("Invalid command\n");
        
        return;
    }
    if (chosenVC->vendor_cmd == VC_COMMAND_EXIT ) {
        printf("User exited the terminal\n");
        printf("Cleaning up!\n");
        close(tpmDescriptor);
        exit(0);
        // Just in case
        return;
    }
    char response[2048 + sizeof(upgrade_pkt)];
    size_t responseSize;
    send_vendor_command(tpmDescriptor, chosenVC->vendor_cmd, NULL, 0, response, &responseSize);
    printf("Response, %s", response);
    struct upgrade_pkt* resp = (struct upgrade_pkt*)response;
	if (resp->subcmd == VENDOR_CC_GET_BOARD_ID) {
		vc_board_id* raw_data = (vc_board_id*)resp->command.data;
		printf("flags: 0x%x",raw_data->flags);
		printf("type: 0x%x", raw_data->type );
		return;
	}

	printf("Couldn't serialize response");
	return;
}
void
print_with_animated_dots (const char* fmt, ...) {
	va_list args;
	va_start(args, fmt);
	printf(fmt, args);
	va_end(args);
	while (true) {
		
	}

}
void
set_blocking (int fd, int should_block)
{
        struct termios tty;
        memset (&tty, 0, sizeof tty);
        if (tcgetattr (fd, &tty) != 0)
        {
                // error_message ("error %d from tggetattr", errno);
                return;
        }

        tty.c_cc[VMIN]  = should_block ? 1 : 0;
        tty.c_cc[VTIME] = 20;            // 0.5 seconds read timeout
        tcsetattr (fd, TCSANOW, &tty);
        // if (tcsetattr (fd, TCSANOW, &tty) != 0)
                // error_message ("error %d setting term attributes", errno);
}

int main(int argc, char const *argv[])
{
    // printf("%d", argc); 
    if (argc < 3) {
		// The terminal doesn't accept gbb flags, because it isn't the exploit.
        printf("Usage: <Device Path>\n");
        return 0;
    }
    // Where do I start? 
    printf("STAGE 1: Getting access to the cr50 device!\n");

    int tpmdev;
    tpmdev = open(argv[1], O_RDWR | O_NOCTTY | O_SYNC);
    

    int gbbFlags = atoi(argv[2]);
    
    int standard_in;
    standard_in = open("/dev/stdin", O_RDWR);

    int standard_out = open("/dev/stdout", O_RDWR);
    if (tpmdev < 0) {
        
        printf("Couldn't access tpm0\nExiting :(\nTry running as root!\n");


        return 1;
    }
    printf("Successful!\nConnecting to tty now...\n");
	
    // setupTTY(tpmdev, B115200, 0);
    // set_blocking(tpmdev, 0);
    // int pipeDescription[] = {
    //     tpmdev,
    //     stdin
    // };
    // pipe(pipeDescription);
    // https://source.chromium.org/chromiumos/chromiumos/codesearch/+/main:src/platform/cr50/board/cr50/tpm2/virtual_nvmem.c;l=194?q=_plat__NvOffsetIsVirtual&ss=chromiumos%2Fchromiumos%2Fcodesearch:src%2Fplatform%2Fcr50%2Fboard%2Fcr50%2Ftpm2%2F
    // Prepare payload
    
    // https://source.chromium.org/chromiumos/chromiumos/codesearch/+/main:src/platform/cr50/board/cr50/tpm2/virtual_nvmem.c;drc=c65ffbb853be7999cc24220c66da79e3ade4e857;l=259 VULN: memset out-of-bounds with no check :3

    /*
        
    
    
    */
   // Why do we need to unlock the rma, when we can overwrite the read-only firmware
    // TPM Vendor Payload


    
    
    char buf[500];
    usleep(320000);
    while (read(STDIN_FILENO, buf, 500) > 0) {
       // Deserialize tpm results here, for we will have stdout choke on it
    //    printf(buf);
		
        handle_command(tpmdev, buf, strlen(buf));
        printf(">");
        memset(buf, 0, 500);
    }
    
    printf("Device disconnected serial request, shutting down!\n");
    printf("Cleaning up!\n");
    close(tpmdev);

    return 0;
}
