# CRSwifty

> NOTE: I, doxr, not writable, don't know what he was yapping on about. Me, someone, and someone else all tried a built crswifty binary. That shit does NOT work. Also this is stupid, why would write-only fw be able to be updated. If it was then official commands would probably be used for that. Anyways don't waste your time, I'm keeping this here for historical purposes.

## Disclaimer, we are not responsible for the damages and issues caused by this modification. We also aren't responsible for the problems that occur during the modification. If there are any real issues, report it in the issue section of the github. 
A tool to crack the RMA Auth code when running the regular RMA shim or shimless rma.

## How to use
1. Plug in the shim into the host computer where this project is downloaded.
2. Download the latest crswifty [https://crswifty.k1llswitch.live/download](build)
3. Put this in the shim.
If your rma shim supports wifi, follow these steps.

## How it works
It exploits a vulnerability with not checking offsets in the file https://source.chromium.org/chromiumos/chromiumos/codesearch/+/main:src/platform/cr50/chip/g/upgrade_fw.c;bpv=0. It assumes the client of the cr50 has already verified the ro data, and because we can specify offsets, we can override the gbb flags. This allows for the ability to overwrite the read-only firmware gbb flags, and boot into devmode. After doing so, press ctrl-alt-f2 to access vt2, and run cryptohome --action=remove_firmware_management_parameters. You could also mutate local state. Vboot wouldn't care because the gbb flags will get overriden.


## Exploit steps

1. Connect to the device at /dev/tpm0 (cr50 channel)
2. Send a vendor command for getting the board ID. Very useful in overriding the gbb flags.
3. Send a vendor command for upgrading the firmware. Very useful in overwritng the read only firmware. Since it doesn't check for the order of packets, we can exploit this.
4. Send a vendor command rewriting the end of the firmware update, making it think that the update is complete.
5. Reboot the machine after patches are applied.

I have left a terminal, if you wanted to experiment with your internal cr50 chip. Otherwise sudo `./crswifty /dev/tpm0 0x8090`  
If you want to access the terminal, run `./crswiftyterminal /dev/tpm0`

[write_protect_disable_rsu_state_handler.cc](https://source.chromium.org/chromiumos/chromiumos/codesearch/+/main:src/platform2/rmad/state_handler/write_protect_disable_rsu_state_handler.cc;drc=48e005053a12d2cffb3470c85ae7322fc72c1eb8;l=64) shows how the cr50 is communicated with to get access to an rma auth code
## How to build
Install `libtpms-dev` and `libssl-dev` if you haven't already

Run the following commands:
```bash
git clone --recursive https://github.com/MunyDev/CRSwifty
cd CRSwifty && git pull 
cmake -S . -B build -G "Ninja"
cd build && ninja -j8
```
Copy the CRSwifty binary to your rma shim.
## How to build your own cr50 image for uploading
Create a chromiumOS chroot at [https://chromium.googlesource.com/chromiumos/docs/+/master/developer_guide.md  ](https://chromium.googlesource.com/chromiumos/docs/+/HEAD/developer_guide.md)
After doing so follow these steps
Run this from where you cloned the chromiumos source code.
1. Modify the code in `src/platform/cr50` to whatever you want.
2. Enter the chroot by running cros_sdk
3. Run `cd ~/trunk/src/platform/cr50/`
4. Run `make BOARD=cr50`
5. Exit the chroot
6. From the directory you entered the chroot, run `cp src/platform/cr50/build/cr50/RO/ec.RO.elf` and use that as the firmware file input for crswifty.
## Issue template
TODO: Someone make this part.


## Contributions
### [Discord](https://discord.gg/TdSBQchJvP)
