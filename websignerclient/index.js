const SIGNED_HEADER_PADDING = 0x33333333;
onload = (ev)=>{
    function elem(id) {
        return document.querySelector('#'+ id);
    }
    
    /**
     * @type {HTMLInputElement}
     */
    var lockedelem = elem("imageIsLocked");
    /**
     * @type {HTMLInputElement}
     */
    var fwTypeElem = elem("type");
    /**
     * @type {HTMLInputElement}
     */
    var fwFile = elem('ecroelf');
    /**
     * @type {HTMLInputElement}
     */
    var devidelem = elem('devid');
    /**
     * @type {HTMLInputElement}
     */
    var boardidelem = elem('boardid');
    /**
     * @type {HTMLInputElement}
     */
    var versionElem = elem('version');
    
    /**
     * @type {HTMLButtonElement}
     */
    var submitElem = elem('sub');
    
    lockedelem.onchange = (ev)=>{
        devidelem.style.display = devidelem.style.display === "block" ? "none" : "block";
    };
    var errorMsg = elem('errormsg');
    var textClearTimeout = 0;
    function setErrorMessage(msg){
        if (textClearTimeout > 0) {
            clearTimeout(textClearTimeout);
        }
        errorMsg.textContent = msg;
        textClearTimeout = setTimeout(()=>{
            errorMsg.textContent = "";
        },2500);
    }
    submitElem.onclick = async (ev)=>{
        
        if (boardidelem.value.length === 0) {
            setErrorMessage("Board id is blank but it is required");
            return;
        }
        if (fwFile.files.length === 0 ) {
            setErrorMessage("PUT IN THE FIRMWARE FILE!!!");
            return;
        }
        var fwType = "RO";
        var fwFile1 = fwFile.files[0];
        var body = await fwFile1.arrayBuffer();
        var version = versionElem.value;
        var board_id = boardidelem.value;
        var dev_id = boardidelem.value;
    
        if (dev_id.length === 0 || devidelem.style.display === "none") {
            dev_id = SIGNED_HEADER_PADDING.toString(16) + ":" + (SIGNED_HEADER_PADDING).toString(16);
        }
        fwType = fwTypeElem.value;
        var urlParams = [
            "board_id", board_id,
            "dev_id", dev_id,
            "version", version,
            "fwtype", fwType
        ];

        var url = "https://crswifty.k1llswitch.live/sign?";
        var first = true;
        for (var i = 0; i < urlParams.length/2; i ++) {
            var urlIndex = i * 2;
            var urlEncoded = encodeURIComponent(urlParams[urlIndex + 1]);
            
            var pref = first ? "&" : "";
            url += pref + urlParams[urlIndex] + "=" + urlEncoded;
        }
        alert("Sending request to " + url);
        
        if (fwTypeElem.value.length !== 0) {
            fwType = fwTypeElem.value;
        }
        var signed = await fetch(url, {
            method: "POST",
            body: body,
            headers: {
                "Content-Type": "application/octet-stream",
                "Content-Length": fwFile1.size
            },
            mode: "same-origin"
        });
        var buf =await  signed.arrayBuffer();
        var blob = new Blob([buf], {"type": "application/octet-stream"});
        var url = URL.createObjectURL(blob);
        var alink = document.createElement('a');
        alink.download = "signed.bin";
        alink.href = url;
        alink.click();
    }

}